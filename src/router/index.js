import Vue from 'vue'
import Router from 'vue-router'
import Brock from '@/components/Brock'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Brock',
      component: Brock
    }
  ],
  mode: 'history',
})
